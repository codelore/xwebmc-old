package ru.pixelsky.webmc.appearance;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;

import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages content in some directory
 */
public class ContentManager {
    private byte[] defaultValue;
    private String fileSuffix;
    private File directory;
    private Logger logger = Logger.getLogger(getClass().getName());

    private LoadingCache<String, Optional<byte[]>> cache;

    public ContentManager(File directory, String fileSuffix) {
        this(directory);
        this.fileSuffix = fileSuffix;
    }

    public ContentManager(File directory) {
        Validate.notNull(directory);
        this.directory = directory;

        buildCache();
    }

    public byte[] getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(byte[] defaultValue) {
        this.defaultValue = defaultValue;
    }

    private void buildCache() {
        cache = CacheBuilder.newBuilder()
                .concurrencyLevel(2)
                .maximumSize(300)
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Optional<byte[]>>() {
                    @Override
                    public Optional<byte[]> load(String s) throws Exception {
                        return Optional.fromNullable(loadFile(s));
                    }
                });
    }

    public boolean store(String filename, byte[] data) {
        // Protect from directories names in filename
        filename = encodeURL(filename);

        if (fileSuffix != null)
            filename += fileSuffix;

        // Defensive copy
        data = Arrays.copyOf(data, data.length);

        File file = new File(directory, filename);
        file.getParentFile().mkdirs();
        OutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(data);
            fos.flush();
            logger.severe("Saved " + file.getAbsolutePath());

            cache.put(filename, Optional.of(data));

            return true;
        } catch (IOException e) {
            logger.log(Level.WARNING, "Error saving file " + filename, e);
            return false;
        } finally {
            IOUtils.closeQuietly(fos);
        }
    }

    public byte[] get(String filename) {
        // Protect from directories names in filename
        filename = encodeURL(filename);

        if (fileSuffix != null)
            filename += fileSuffix;

        try {
            return defaultValue != null ? cache.get(filename).or(defaultValue) : cache.get(filename).orNull();
        } catch (ExecutionException e) {
            return null;
        }
    }

    public byte[] loadFile(String filename) {
        File file = new File(directory, filename);
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            IOUtils.copy(is, bos);
            return bos.toByteArray();
        } catch (IOException e) {
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    private String encodeURL(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("No UTF-8");
        }
    }
}
