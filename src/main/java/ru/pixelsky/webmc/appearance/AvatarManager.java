package ru.pixelsky.webmc.appearance;

import org.h2.util.IOUtils;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.utils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AvatarManager {
    private byte[] defaultAvatar;

    public AvatarManager() {
        try {
            InputStream in = getClass().getResourceAsStream("/default/avatar.png");
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            IOUtils.copy(in, buf);
            defaultAvatar = buf.toByteArray();
        } catch (IOException ex) {
            throw new AssertionError("No default avatar");
        }
    }

    public byte[] getBigAvatar(String playerName) {
        BufferedImage skinImage = getSkinImage(playerName);
        if (skinImage != null) {
            BufferedImage frontSkin = ImageUtils.toSize(SkinUtils.getFrontSkin(skinImage), 96, 240);
            return ImageUtils.imageToBytes(frontSkin, "PNG");
        } else {
            return defaultAvatar;
        }
    }

    public byte[] getAvatarImage(String playerName) {
        BufferedImage headImage = getHeadImage(playerName);
        if (headImage != null) {
            BufferedImage sizedHead = ImageUtils.toSize(headImage, 48, 48);
            return ImageUtils.imageToBytes(sizedHead, "PNG");
        } else {
            return defaultAvatar;
        }
    }

    private BufferedImage getHeadImage(String playerName) {
        BufferedImage skinImage = getSkinImage(playerName);
        if (skinImage != null) {
            return SkinPart.HEAD_FRONT.getPartImage(skinImage);
        } else {
            return null;
        }
    }

    private BufferedImage getSkinImage(String playerName) {
        byte[] skinBytes = CommonData.getSkinCM().get(playerName);
        if (skinBytes != null) {
            return ImageUtils.imageFromBytes(skinBytes);
        } else {
            return null;
        }
    }
}
