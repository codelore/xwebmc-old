package ru.pixelsky.webmc;

public class CabinetException extends Exception {
    private CabinetError error;

    public CabinetException(CabinetError error) {
        this.error = error;
    }

    public CabinetException(CabinetError error, String message) {
        super(error.getDescription() + ": " + message);
        this.error = error;
    }

    public CabinetError getError() {
        return error;
    }
}
