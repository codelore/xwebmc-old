package ru.pixelsky.webmc;

import org.apache.commons.lang3.Validate;
import ru.pixelsky.webmc.appearance.AvatarManager;
import ru.pixelsky.webmc.appearance.ContentManager;
import ru.pixelsky.webmc.auth.Authorizator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class CommonData {
    private static EntityManagerFactory entityManagerFactory;
    private static Authorizator authorizator;

    private static ContentManager skinContentManager;
    private static ContentManager cloakContentManager;
    private static AvatarManager avatarManager;

    public static synchronized void setEntityManagerFactory(EntityManagerFactory factory) {
        Validate.notNull(factory);
        entityManagerFactory = factory;
    }

    public static synchronized void setAuthorizator(Authorizator auth) {
        Validate.notNull(auth);
        authorizator = auth;
    }

    public static synchronized void setAvatarManager(AvatarManager am) {
        Validate.notNull(am);
        avatarManager = am;
    }

    public static synchronized void setSkinCloakCM(ContentManager skinCm, ContentManager cloakCm) {
        Validate.notNull(skinCm);
        Validate.notNull(cloakCm);
        skinContentManager = skinCm;
        cloakContentManager = cloakCm;
    }

    public static EntityManager createEntityManager() {
        if (entityManagerFactory == null) {
            synchronized (CommonData.class) {
                // Synchronize variables
            }
        }
        return entityManagerFactory.createEntityManager();
    }

    public static Authorizator getAuthorizator() {
        if (authorizator == null) {
            synchronized (CommonData.class) {
                // Synchronize variables
            }
        }
        return authorizator;
    }

    public static ContentManager getSkinCM() {
        if (skinContentManager == null) {
            synchronized (CommonData.class) {
                // Synchronize variables
            }
        }
        return skinContentManager;
    }

    public static ContentManager getCloakCM() {
        if (cloakContentManager == null) {
            synchronized (CommonData.class) {
                // Synchronize variables
            }
        }
        return cloakContentManager;
    }

    public static AvatarManager getAvatarManager() {
        if (avatarManager == null) {
            synchronized (CommonData.class) {
                // Synchronize variables
            }
        }
        return avatarManager;
    }
}
