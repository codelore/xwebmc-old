package ru.pixelsky.webmc.utils;

import org.apache.commons.lang3.StringEscapeUtils;
import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.User;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ResponseUtils {
    public static void sendJsonHeaders(HttpServletResponse response) {
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "http://pixelsky.ru");
        response.setHeader("Access-Control-Allow-Credentials", "true");
    }

    public static void sendPngImageHeader(HttpServletResponse response) {
        response.setContentType("image/png; charset=UTF-8");
    }

    public static void writeErrorResponse(HttpServletResponse response, CabinetError error) throws IOException {
        response.getWriter().print("{\"error\": {\"code\": " + error.getCode() + ", \"description\": \"" +
                StringEscapeUtils.escapeHtml4(error.getDescription()) + "\"}}");
    }

    public static void writeErrorResponse(HttpServletResponse response, CabinetError error, String data) throws IOException {
        response.getWriter().print("{\"error\": {\"code\": " + error.getCode() + ", \"description\": \"" +
                StringEscapeUtils.escapeHtml4(error.getDescription() + ": " + data) + "\"}}");
    }

    public static void writeSuccess(HttpServletResponse response) throws IOException {
        response.getWriter().print("{\"success\": true}");
    }

    public static User checkAuthorized(HttpServletRequest request, HttpServletResponse response) throws CabinetException {
        HttpSession session =  request.getSession();
        User user;
        if(session == null || (user = CommonData.getAuthorizator().getUser(session)) == null) {
            throw new CabinetException(CabinetError.NOT_AUTHORIZED);
        }
        return user;
    }

    public static User checkAuthorizedAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException, CabinetException {
        HttpSession session =  request.getSession();
        User user;
        if(session == null || (user = CommonData.getAuthorizator().getUser(session)) == null) {
            throw new CabinetException(CabinetError.NOT_AUTHORIZED);
        } else if (!user.isAdmin()) {
            throw new CabinetException(CabinetError.ACCESS_DENIED);
        } else {
            return user;
        }
    }
}
