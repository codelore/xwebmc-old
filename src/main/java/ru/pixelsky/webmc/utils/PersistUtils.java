package ru.pixelsky.webmc.utils;

import ru.pixelsky.webmc.User;

import javax.persistence.EntityManager;
import java.util.List;

public class PersistUtils {
    @SuppressWarnings("unchecked")
    public static User loadUser(EntityManager em, long userId) {
        List<User> users = em.createQuery("from users where uid=:id").setParameter("id", userId).getResultList();
        return users.size() > 0 ? users.get(0) : null;
    }
}
