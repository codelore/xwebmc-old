package ru.pixelsky.webmc.servlet;

import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.auth.Authorizator;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends CabinetServlet {
    @Override
    protected void handlePost(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        ResponseUtils.sendJsonHeaders(resp);

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        doLogin(req, login, password);

        ResponseUtils.writeSuccess(resp);
    }

    private void doLogin(HttpServletRequest req, String username, String password) throws CabinetException {
        if (username == null || password == null)
            throw new CabinetException(CabinetError.BAD_LOGIN_PASSWORD);

        Authorizator authorizator = CommonData.getAuthorizator();
        User user = authorizator.login(req.getSession(true), username, password);
        if (user == null) {
            throw new CabinetException(CabinetError.BAD_LOGIN_PASSWORD);
        }
    }
}
