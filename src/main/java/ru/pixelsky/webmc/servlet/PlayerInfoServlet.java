package ru.pixelsky.webmc.servlet;

import com.google.gson.JsonObject;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class PlayerInfoServlet extends CabinetServlet {
    private final static long MILLIS_IN_DAY = TimeUnit.DAYS.toMillis(1);

    @Override
    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException {
        ResponseUtils.sendJsonHeaders(resp);
        User user = ResponseUtils.checkAuthorized(req, resp);

        JsonObject object = new JsonObject();
        object.addProperty("name", user.getLogin());
        object.addProperty("realmoney", user.getRealmoney());
        object.addProperty("avatar", "content/avatars/" + user.getLogin() + ".png");
        object.addProperty("is_admin", user.isAdmin());
        object.addProperty("karma", user.getKarma());
        object.add("money", buildMoneyObject(user));
        object.add("votes", buildVotesObject(user));
        resp.getWriter().print(object.toString());
    }

    private JsonObject buildMoneyObject(User user) {
        JsonObject moneyObject = new JsonObject();
        moneyObject.addProperty("freebuild", 0x10c);
        moneyObject.addProperty("rpg", 0xc01);
        return moneyObject;
    }

    private JsonObject buildVotesObject(User user) {
        long now = System.currentTimeMillis();
        boolean w2vEnabled = now > user.getLastVoteW2V() + MILLIS_IN_DAY;
        boolean mctopEnabled = now > user.getLastVoteMcTop() + MILLIS_IN_DAY;

        JsonObject object = new JsonObject();
        object.addProperty("mctop", mctopEnabled);
        object.addProperty("w2v", w2vEnabled);

        return object;
    }
}
