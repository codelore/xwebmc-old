package ru.pixelsky.webmc.servlet;

import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Saves files somewhere on disk
 */
public class ContentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String servletPref = req.getContextPath() + req.getServletPath();
        String uri = req.getRequestURI();

        if (uri.length() <= servletPref.length()) {
            // Strange path
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        // uri = servletPref + "/" + filename
        String filename = uri.substring(servletPref.length() + 1);

        if (filename.endsWith(".png")) {
            // We should pass player login to ContentManager
            filename = filename.substring(0, filename.length() - ".png".length());
        }

        byte[] encodedImage = null;
        if (filename.startsWith("skins/")) {
            String playerName = filename.substring("skins/".length());
            encodedImage = CommonData.getSkinCM().get(playerName);
        }
        if (filename.startsWith("cloaks/")) {
            String playerName = filename.substring("cloaks/".length());
            encodedImage = CommonData.getCloakCM().get(playerName);
        }
        if (filename.startsWith("avatars/")) {
            String playerName = filename.substring("avatars/".length());
            encodedImage = CommonData.getAvatarManager().getAvatarImage(playerName);
        }
        if (filename.startsWith("frontskins/")) {
            String playerName = filename.substring("frontskins/".length());
            encodedImage = CommonData.getAvatarManager().getBigAvatar(playerName);
        }

        if (encodedImage != null) {
            ResponseUtils.sendPngImageHeader(resp);
            resp.getOutputStream().write(encodedImage);
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
