package ru.pixelsky.webmc.servlet;

import org.apache.commons.lang3.math.NumberUtils;
import ru.pixelsky.webmc.*;
import ru.pixelsky.webmc.shop.ShopCartItem;
import ru.pixelsky.webmc.shop.ShopItem;
import ru.pixelsky.webmc.utils.PersistUtils;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShopBuyServlet extends CabinetServlet {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException {
        ResponseUtils.sendJsonHeaders(resp);

        User user = ResponseUtils.checkAuthorized(req, resp);

        int itemId = NumberUtils.toInt(req.getParameter("id"), -1);
        int count = NumberUtils.toInt(req.getParameter("count"), -1);
        long totalPrice = NumberUtils.toLong(req.getParameter("totalPrice"), -1);

        // Check parameters
        if (itemId <= 0) {
            throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "itemId");
        }
        if (count <= 0) {
            throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "count");
        }
        if (totalPrice <= 0) {
            throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "totalPrice");
        }

        EntityManager manager = CommonData.createEntityManager();
        try {
            ShopItem shopItem = getShopItemById(itemId);
            // Checking amount
            if (shopItem.getPrice() * count == totalPrice) {
                /* Transaction start */
                manager.getTransaction().begin();

                user = PersistUtils.loadUser(manager, user.getId());
                if (user.takeRealmoney(totalPrice)) {
                    ShopCartItem cartItem = ShopCartItem.valueOf(shopItem, user.getLogin(), count);
                    manager.persist(cartItem);
                    manager.persist(user);
                    manager.getTransaction().commit();

                    resp.getOutputStream().print("{\"success:\" true}");
                } else {
                    manager.getTransaction().rollback();
                }
                /* Transaction end */
            } else {
                throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "totalPrice");
            }
        } finally {
            manager.close();
        }
    }

    @SuppressWarnings("unchecked")
    private ShopItem getShopItemById(long itemId) {
        EntityManager manager = CommonData.createEntityManager();
        List<ShopItem> items = manager.createQuery("from shop_items").getResultList();
        manager.close();
        if (items.size() > 0)
            return items.get(0);
        else
            return null;
    }
}
