package ru.pixelsky.webmc.servlet;

import org.h2.util.IOUtils;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.appearance.AvatarManager;
import ru.pixelsky.webmc.appearance.ContentManager;
import ru.pixelsky.webmc.auth.xf.XFAuthorizator;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class InitServlet extends HttpServlet {
    private boolean isInit;

    @Override
    public synchronized void init() throws ServletException {
        if (isInit)
            return;

        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("webmc");
        CommonData.setEntityManagerFactory(emFactory);

        CommonData.setAuthorizator(new XFAuthorizator(emFactory));

        File contentsDir = new File("contents");
        File skinsDir = new File(contentsDir, "skins");
        File cloaksDir = new File(contentsDir, "cloaks");

        ContentManager skinsCM = new ContentManager(skinsDir, ".png");
        skinsCM.setDefaultValue(getResource("/default/skin.png"));
        CommonData.setSkinCloakCM(skinsCM, new ContentManager(cloaksDir, ".png"));
        CommonData.setAvatarManager(new AvatarManager());

        isInit = true;
    }

    private byte[] getResource(String resource) {
        try {
            InputStream in = getClass().getResourceAsStream(resource);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            IOUtils.copy(in, buf);
            return buf.toByteArray();
        } catch (IOException ex) {
            throw new AssertionError("No resource: " + resource);
        }
    }
}
