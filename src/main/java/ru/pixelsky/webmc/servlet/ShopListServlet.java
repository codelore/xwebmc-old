package ru.pixelsky.webmc.servlet;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.utils.ResponseUtils;
import ru.pixelsky.webmc.shop.ShopItem;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShopListServlet extends CabinetServlet {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseUtils.sendJsonHeaders(resp);
            List<ShopItem> itemList = getShopItemsList();
            JsonArray jsonItemList = new JsonArray();

            for (ShopItem shopItem : itemList) {
                jsonItemList.add(shopItem.asJSON());
            }

            JsonObject responseObject = new JsonObject();
            responseObject.add("list", jsonItemList);

            resp.getWriter().print(responseObject.toString());
    }

    @SuppressWarnings("unchecked")
    private List<ShopItem> getShopItemsList() {
        EntityManager manager = CommonData.createEntityManager();
        try {
            List<ShopItem> items = manager.createQuery("from shop_items").getResultList();
            return items;
        } finally {
            manager.close();
        }
    }
}
