package ru.pixelsky.webmc.servlet;

import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.auth.Authorizator;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends CabinetServlet {
    @Override
    protected void handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseUtils.sendJsonHeaders(resp);
        CommonData.getAuthorizator().logout(req.getSession());
        ResponseUtils.writeSuccess(resp);
    }
}
