package ru.pixelsky.webmc.servlet;

import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CabinetServlet extends HttpServlet {
    private Logger cabinetLogger = Logger.getLogger("Cabinet");

    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws Exception {

    }

    protected void handlePost(HttpServletRequest req, HttpServletResponse resp) throws Exception {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseUtils.sendJsonHeaders(resp);
        try {
            handleGet(req, resp);
        } catch (Throwable ex) {
            handleException(req, resp, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseUtils.sendJsonHeaders(resp);
        try {
            handlePost(req, resp);
        } catch (Throwable ex) {
            handleException(req, resp, ex);
        }
    }

    private void handleException(HttpServletRequest req, HttpServletResponse resp, Throwable ex) throws IOException {
        if (ex instanceof CabinetException) {
            CabinetException c = (CabinetException) ex;
            ResponseUtils.writeErrorResponse(resp, c.getError(), c.getMessage());
        } else {
            logException(req, ex);
            ResponseUtils.writeErrorResponse(resp, CabinetError.UNKNOWN_ERROR, "Unknown error. See server log for details.");
        }
    }

    private void logException(HttpServletRequest request, Throwable ex) {
        cabinetLogger.log(Level.SEVERE, "Exception occurred. Method: " + request.getMethod() +
                " Path: " + request.getPathInfo() + ", IP: " + request.getRemoteHost(), ex);
    }
}
