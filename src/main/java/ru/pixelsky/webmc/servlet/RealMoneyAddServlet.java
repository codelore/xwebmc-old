package ru.pixelsky.webmc.servlet;

import org.apache.commons.lang3.math.NumberUtils;
import org.h2.util.StringUtils;
import ru.pixelsky.webmc.*;
import ru.pixelsky.webmc.utils.PersistUtils;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RealMoneyAddServlet extends CabinetServlet {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException {
        ResponseUtils.sendJsonHeaders(resp);

        User user = ResponseUtils.checkAuthorizedAdmin(req, resp);

        long count = NumberUtils.toLong(req.getParameter("count"), 0);
        if (count == 0) {
            throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "count");
        }

        EntityManager manager = CommonData.createEntityManager();
        try {
            manager.getTransaction().begin();

            user = PersistUtils.loadUser(manager, user.getId());
            user.setRealmoney(user.getRealmoney() + count);
            manager.persist(user);

            manager.getTransaction().commit();

            ResponseUtils.writeSuccess(resp);
        } finally {
            manager.close();
        }
    }
}
