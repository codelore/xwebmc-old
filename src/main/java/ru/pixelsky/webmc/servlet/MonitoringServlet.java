package ru.pixelsky.webmc.servlet;

import com.google.gson.JsonArray;
import ru.pixelsky.webmc.monitoring.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Responds with json monitoring data
 */
public class MonitoringServlet extends HttpServlet {
    private MonitoringDaemon daemon = new MonitoringDaemonImpl(Arrays.asList(
            new MonitoredServer("10.0.0.1", 25565, "Pixelsky | Main")
    ));
    private Logger logger = Logger.getLogger(getClass().getName());

    public MonitoringServlet() {
        daemon.start();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        resp.setHeader("Access-Control-Allow-Origin", "*");

        if (daemon == null) {
            logger.warning("No monitoring daemon specified");
            resp.sendError(500);
            return;
        }

        MonitoringResult result = daemon.getMonitoringResult();
        JsonArray array = new JsonArray();
        if (result != null) {
            ServerState[] states = result.getServerStates();
            for (ServerState state : states) {
                array.add(state.asJson());
            }
        }
        resp.getWriter().write(array.toString());
    }
}
