package ru.pixelsky.webmc.servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.appearance.ContentManager;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContentUploadServlet extends CabinetServlet {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException, FileUploadException {
        ResponseUtils.sendJsonHeaders(resp);

        User user = ResponseUtils.checkAuthorized(req, resp);

            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);
            for (FileItem item : items) {
                if (!item.isFormField() && item.getSize() > 0) {
                    String fieldname = item.getFieldName();
                    if (!upload(fieldname, item.get(), user.getLogin())) {
                        throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, fieldname);
                    }
                }
            }

            ResponseUtils.writeSuccess(resp);
    }

    private boolean upload(String fieldName, byte[] bytes, String playerName) throws IOException {
        ContentManager usedCm = null;
        if (fieldName.equals("skin"))
            usedCm = CommonData.getSkinCM();
        if (fieldName.equals("cloak"))
            usedCm = CommonData.getCloakCM();
        if (usedCm == null) {
            return false;
        }

        Dimension dimension = getImageDimension(bytes);
        if (checkDimension(dimension)) {
            bytes = toPNG(bytes);
            usedCm.store(playerName, bytes);
            return true;
        } else {
            return false;
        }
    }

    private byte[] toPNG(byte[] imageInSomeFormat) throws IOException {
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageInSomeFormat));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "PNG", bos);
        return bos.toByteArray();
    }

    private boolean checkDimension(Dimension dimension) {
        if (dimension == null)
            return false;
        int w = (int) dimension.getWidth();
        int h = (int) dimension.getHeight();
        return w == 2 * h && (w == 64 || w == 128 || w == 256 || w == 512 || w == 1024);
    }

    private Dimension getImageDimension(byte[] bytes) throws IOException {
        InputStream stream = new ByteArrayInputStream(bytes);
        ImageInputStream in = ImageIO.createImageInputStream(stream);
        final Iterator readers = ImageIO.getImageReaders(in);
        while (readers.hasNext()) {
            ImageReader reader = (ImageReader) readers.next();
            try {
                logger.severe("Trying reader " + reader.getFormatName());
                reader.setInput(in);
                return new Dimension(reader.getWidth(0), reader.getHeight(0));
            } finally {
                reader.dispose();
            }
        }
        return null;
    }
}
