package ru.pixelsky.webmc.servlet;

import com.google.gson.JsonObject;
import ru.pixelsky.webmc.CabinetError;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.shop.ShopCartItem;
import ru.pixelsky.webmc.shop.ShopItem;
import ru.pixelsky.webmc.utils.PersistUtils;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.persistence.EntityManager;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class VoteServlet extends CabinetServlet {
    private final static long MILLIS_IN_DAY = TimeUnit.DAYS.toMillis(1);

    private String urlW2V;
    private String urlMctop;

    @Override
    public void init(ServletConfig config) throws ServletException {
        urlW2V = config.getInitParameter("w2v");
        urlMctop = config.getInitParameter("mctop");
    }

    @Override
    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException {
        User user = ResponseUtils.checkAuthorized(req, resp);

        Rating rating = parseRating(req.getParameter("rating"));

        EntityManager manager = CommonData.createEntityManager();
        try {
            /* Transaction start */
            manager.getTransaction().begin();

            user = PersistUtils.loadUser(manager, user.getId());

            long now = System.currentTimeMillis();
            switch (rating) {
                case W2V:
                    if (user.getLastVoteW2V() + MILLIS_IN_DAY < now) {
                        resp.sendRedirect(urlW2V);
                        user.setKarma(user.getKarma() + 1);
                        user.setLastVoteW2V(now);
                    } else {
                        throw new CabinetException(CabinetError.ALREADY_VOTED);
                    }
                    break;
                case MCTOP:
                    if (user.getLastVoteMcTop() + MILLIS_IN_DAY < now) {
                        resp.sendRedirect(urlMctop);
                        user.setKarma(user.getKarma() + 1);
                        user.setLastVoteMcTop(now);
                    } else {
                        throw new CabinetException(CabinetError.ALREADY_VOTED);
                    }
                    break;
            }

            manager.getTransaction().commit();
            /* Transaction end */
        } finally {
            manager.close();
        }
    }

    private Rating parseRating(String param) throws CabinetException {
        if (param == null)
            throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "rating");

        switch (param) {
            case "w2v":
                return Rating.W2V;
            case "mctop":
                return Rating.MCTOP;
            default:
                throw new CabinetException(CabinetError.ILLEGAL_ARGUMENT, "rating");
        }
    }

    private enum Rating {
        W2V, MCTOP
    }
}
