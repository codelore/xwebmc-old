package ru.pixelsky.webmc.servlet;

import com.google.gson.JsonObject;
import ru.pixelsky.webmc.CabinetException;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.User;
import ru.pixelsky.webmc.utils.ResponseUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SkinCloakGetServlet extends CabinetServlet {
    @Override
    protected void handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, CabinetException {
        ResponseUtils.sendJsonHeaders(resp);
        User user = ResponseUtils.checkAuthorized(req, resp);

        JsonObject object = new JsonObject();

        byte[] skinData = CommonData.getSkinCM().get(user.getLogin());
        byte[] cloakData = CommonData.getCloakCM().get(user.getLogin());

        if (cloakData != null)
            object.addProperty("cloak", "content/cloaks/" + user.getLogin() + ".png");

        if (skinData != null) {
            object.addProperty("skin", "content/skins/" + user.getLogin() + ".png");
            object.addProperty("frontskin", "content/frontskins/" + user.getLogin() + ".png");
        }

        resp.getWriter().println(object.toString());
    }
}
