package ru.pixelsky.webmc;

import org.apache.commons.lang3.Validate;

import javax.persistence.*;

@Entity(name = "users")
public class User {
    private Long id;
    private String login;
    private long realmoney;
    private long lastVoteMcTop;
    private long lastVoteW2V;
    private int karma;

    private boolean isAdmin;

    public User(String login) {
        this.login = login;
    }

    public User() {
    }

    @Column(name = "uid")
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "realmoney")
    public long getRealmoney() {
        return realmoney;
    }

    public void setRealmoney(long realmoney) {
        this.realmoney = realmoney;
    }

    @Column(name = "is_admin")
    public boolean isAdmin() {
        return isAdmin;
    }

    @Column(name = "vote_mctop_last")
    public long getLastVoteMcTop() {
        return lastVoteMcTop;
    }

    public void setLastVoteMcTop(long lastVoteMcTop) {
        this.lastVoteMcTop = lastVoteMcTop;
    }

    @Column(name = "vote_w2v_last")
    public long getLastVoteW2V() {
        return lastVoteW2V;
    }

    public void setLastVoteW2V(long lastVoteW2V) {
        this.lastVoteW2V = lastVoteW2V;
    }

    @Column(name = "karma")
    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean takeRealmoney(long amount) {
        if (amount <= 0)
            throw new IllegalArgumentException("amount must be positive");
        if (realmoney >= amount) {
            setRealmoney(realmoney - amount);
            return true;
        }
        else
            return false;
    }

    @Override
    public String toString() {
        return login +" (#" + id + ")";
    }
}
