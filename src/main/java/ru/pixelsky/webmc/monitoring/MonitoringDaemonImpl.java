package ru.pixelsky.webmc.monitoring;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

public final class MonitoringDaemonImpl implements MonitoringDaemon {
    private final static int TIMEOUT = 1000;
    private final static long PERIOD = 10000;
    private final static long CONNECTION_DROP_IGNORED = 30000;

    private List<MonitoredServer> monitoredServers;
    private boolean running;
    private MonitoringResult result;

    public MonitoringDaemonImpl(List<MonitoredServer> serverList) {
        this.monitoredServers = new ArrayList<MonitoredServer>(serverList);
    }

    public synchronized void start() {
        if (running)
            throw new IllegalStateException("Already running");
        running = true;
        Thread daemonThread = new Thread(new Runnable() {
            @Override
            public void run() {
                MonitoringDaemonImpl.this.run();
            }
        }, "Server monitoring daemon");
        daemonThread.setDaemon(true);
        daemonThread.start();
    }

    public synchronized void stop() {
        synchronized (this) {
            if (!running)
                throw new IllegalStateException("Already stopped");
            running = false;
        }
    }

    @Override
    public void setServersList(List<MonitoredServer> serversList) {
        Validate.notNull(serversList);
        synchronized (this) {
            this.monitoredServers = serversList;
        }
    }

    @Override
    public MonitoringResult getMonitoringResult() {
        return result;
    }

    private void run() {
        boolean alive = true;
        while (alive) {
            try {
                Thread.sleep(PERIOD);
            } catch (InterruptedException ignored) {

            }
            update();
            synchronized (this) {
                alive = running;
            }
        }
    }

    private void update() {
        ServerState[] states = new ServerState[monitoredServers.size()];
        for (int i = 0; i < states.length; i++) {
            states[i] = updateServer(monitoredServers.get(i));
            if (!states[i].isOnline() && result != null) {
                // Ignore dropping connection for CONNECTION_DROP_IGNORED ms
                ServerState oldState = result.getServerStates()[i];
                if (oldState.isOnline()) {
                    if (states[i].getUpdateDate() < oldState.getUpdateDate() + CONNECTION_DROP_IGNORED) {
                        // Less than CONNECTION_DROP_IGNORED ms since server online
                        states[i] = oldState;
                    }
                }
            }
        }

        synchronized (this) {
            result = new MonitoringResult(states);
        }
    }

    /**
     * Copied from minecraft code
     */
    private ServerState updateServer(MonitoredServer serverData)
    {
        ServerState state = new ServerState();
        Socket socket = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        state.setServer(serverData);
        state.setUpdateDate(System.currentTimeMillis());

        try
        {
            socket = new Socket();
            socket.setSoTimeout(3000);
            socket.setTcpNoDelay(true);
            socket.setTrafficClass(18);
            socket.connect(new InetSocketAddress(serverData.getHost(), serverData.getPort()), TIMEOUT);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            dos.write(254);
            dos.write(1);

            if (dis.read() != 255)
            {
                throw new IOException("Bad message");
            }

            String s = readString(dis, 256);
            String[] astring;

            if (s.startsWith("\u00a7") && s.length() > 1)
            {
                astring = s.substring(1).split("\u0000");

                if (NumberUtils.toInt(astring[0], 0) == 1)
                {
                    state.setMotd(astring[3]);
                    state.setVersion(astring[2]);
                    state.setPlayersCount(NumberUtils.toInt(astring[4], 0));
                    state.setPlayersCountMax(NumberUtils.toInt(astring[5], 0));
                }
                else
                {

                }
            }
            else
            {
                astring = s.split("\u00a7");
                state.setMotd(astring[0]);
                state.setPlayersCount(NumberUtils.toInt(astring[1]));
                state.setPlayersCountMax(NumberUtils.toInt(astring[2]));
                state.setVersion("1.3");
            }
            state.setOnline(true);
        } catch (IOException ignored) {
            state.setMotd(serverData.getName());
        } finally
        {
            IOUtils.closeQuietly(dis);
            IOUtils.closeQuietly(dos);
            closeSocketQuietly(socket);
        }
        return state;
    }

    /**
     * Copied from minecraft
     */
    public static String readString(DataInputStream input, int maxLength) throws IOException
    {
        short short1 = input.readShort();

        if (short1 > maxLength)
        {
            throw new IOException("Received string length longer than maximum allowed (" + short1 + " > " + maxLength + ")");
        }
        else if (short1 < 0)
        {
            throw new IOException("Received string length is less than zero! Weird string!");
        }
        else
        {
            StringBuilder stringbuilder = new StringBuilder();

            for (int j = 0; j < short1; ++j)
            {
                stringbuilder.append(input.readChar());
            }

            return stringbuilder.toString();
        }
    }

    private void closeSocketQuietly(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ignored) {

            }
        }
    }

}