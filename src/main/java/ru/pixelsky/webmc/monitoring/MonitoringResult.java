package ru.pixelsky.webmc.monitoring;

import java.util.Arrays;

public class MonitoringResult {
    private ServerState[] servers;

    public MonitoringResult(ServerState[] servers) {
        this.servers = Arrays.copyOf(servers, servers.length);
    }

    public ServerState[] getServerStates() {
        return servers;
    }
}
