package ru.pixelsky.webmc.monitoring;

import com.google.gson.JsonObject;

public class ServerState {
    /**
     * Player count
     */
    protected int playersCount;
    protected int playersTotal;
    /**
     * Server is ups
     */
    protected boolean online;
    protected MonitoredServer server;
    protected String motd;
    private String version;
    private long updateDate;
    private long lastOnline;

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public MonitoredServer getServer() {
        return server;
    }

    public void setServer(MonitoredServer server) {
        this.server = server;
    }

    public int getPlayersCount() {
        return playersCount;
    }

    public void setPlayersCount(int playersCount) {
        this.playersCount = playersCount;
    }

    public int getPlayersTotal() {
        return playersTotal;
    }

    public void setPlayersCountMax(int playersTotal) {
        this.playersTotal = playersTotal;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getMotd() {
        return motd;
    }

    public void setMotd(String motd) {
        this.motd = motd;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(long lastOnline) {
        this.lastOnline = lastOnline;
    }

    public JsonObject asJson() {
        JsonObject object = new JsonObject();
        object.addProperty("playersCount", playersCount);
        object.addProperty("playersMax", playersTotal);
        object.addProperty("motd", motd);
        object.addProperty("version", version);
        object.addProperty("online", online);
        object.addProperty("updateDate", updateDate);
        return object;
    }
}
