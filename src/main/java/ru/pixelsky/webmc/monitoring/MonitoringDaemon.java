package ru.pixelsky.webmc.monitoring;

import java.util.List;

public interface MonitoringDaemon {
    public void start();
    public void stop();
    public void setServersList(List<MonitoredServer> serversList);
    public MonitoringResult getMonitoringResult();
}
