package ru.pixelsky.webmc;

public enum CabinetError {
    UNKNOWN_ERROR(1, "Неизвестная ошибка"),
    ILLEGAL_ARGUMENT(2, "Неверный параметр"),
    CONTROL_MISMATCH(3, "Неверное проверочное значение"),
    NOT_AUTHORIZED(4, "Не авторизован"),
    NOT_ENOUGH_MONEY(5, "Не хватает денег на счете"),
    BAD_IMAGE(6, "Недопустимое изображение"),
    BAD_LOGIN_PASSWORD(7, "Неверная пара логин/пароль"),
    ALREADY_VOTED(8, "Уже проголосовали"),
    ACCESS_DENIED(42, "Вы не избранный");

    private int code;
    private String description;

    private CabinetError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
