package ru.pixelsky.webmc.auth;

import ru.pixelsky.webmc.User;

import javax.servlet.http.HttpSession;

/**
 * Represents how login and password are checked and how user identification is stored in session
 * Authorizator is responsible for mapping logins to user ids
 */
public interface Authorizator {
    public User getUser(HttpSession session);
    public User login(HttpSession session, String login, String password);
    public void logout(HttpSession session);
}