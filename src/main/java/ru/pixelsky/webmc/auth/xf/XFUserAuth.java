package ru.pixelsky.webmc.auth.xf;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "xf_user_authenticate")
public class XFUserAuth {
    private String id;
    private String data;

    @Id
    @Column(name = "user_id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "XFUserAuth{" +
                "id='" + id + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
