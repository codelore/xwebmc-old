package ru.pixelsky.webmc.auth.xf;

import org.apache.commons.lang3.Validate;
import ru.pixelsky.webmc.CommonData;
import ru.pixelsky.webmc.utils.PersistUtils;
import ru.pixelsky.webmc.auth.Authorizator;
import ru.pixelsky.webmc.utils.CryptUtils;
import ru.pixelsky.webmc.User;

import javax.persistence.*;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XFAuthorizator implements Authorizator {
    private Logger logger = Logger.getLogger(getClass().getName());

    public XFAuthorizator(EntityManagerFactory webmc) {
        Validate.notNull(webmc);
    }

    @Override
    public User login(HttpSession session, String login, String password) {
        setUser(session, null);
        if (checkPassword(login, password)) {
            User user = getUser(login);
            setUser(session, user);
            return user;
        }
        return null;
    }

    @Override
    public void logout(HttpSession session) {
        session.removeAttribute("userId");
    }

    @Override
    public User getUser(HttpSession session) {
        Long userId = (Long) session.getAttribute("userId");
        if (userId != null) {
            EntityManager em = CommonData.createEntityManager();
            try {
                User user = PersistUtils.loadUser(em, userId);
                return user;
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error loading user data: " + userId, ex);
                return null;
            } finally {
                em.close();
            }
        } else {
            return null;
        }

    }

    private void setUser(HttpSession session, User user) {
        if (user != null && user.getId() != null)
            session.setAttribute("userId", user.getId());
        else
            session.removeAttribute("userId");
    }

    @SuppressWarnings("unchecked")
    private User getUser(String login) {
        EntityManager usersEM = CommonData.createEntityManager();
        try {
            List<User> users = usersEM.createQuery("from users u where u.login=:login").setParameter("login", login).getResultList();
            if (users.isEmpty()) {
                try {
                    User user = new User(login);
                    usersEM.getTransaction().begin();
                    usersEM.persist(user);
                    usersEM.getTransaction().commit();
                    return user;
                } catch (RollbackException ex) {
                    logger.log(Level.WARNING, "User register transaction was rolled back", ex);
                    return null;
                }
            } else {
                return users.get(0);
            }

        } finally {
            usersEM.close();
        }
    }

    private boolean checkPassword(String username, String password) {
        String saltPrefix = "\"salt\";s:64:\"";
        String hashPrefix = "\"hash\";s:64:\"";

        String data = getXFDataForUsername(username);
        if (data == null) {
            // No such username
            return false;
        }
        int saltStart = data.indexOf(saltPrefix) + saltPrefix.length();
        int hashStart = data.indexOf(hashPrefix) + hashPrefix.length();

        String dataHash = data.substring(hashStart, hashStart + 64);
        String dataSalt = data.substring(saltStart, saltStart + 64);

        String expectedhash = CryptUtils.sha256(CryptUtils.sha256(password) + dataSalt);
        return expectedhash.equals(dataHash);
    }

    @SuppressWarnings("unchecked")
    private String getXFDataForUsername(String username) {
        EntityManager manager = CommonData.createEntityManager();
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            List<XFUserData> usersById = manager.createQuery("from xf_user where username=:name")
                    .setParameter("name", username).getResultList();
            if (usersById.size() > 0) {
                XFUserData data = usersById.get(0);
                return getXFDataForUserId(manager, data.getId());
            }
        } finally {
            manager.close();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private String getXFDataForUserId(EntityManager authEM, long uid) {
        List<XFUserAuth> authDatas = authEM.createQuery("from xf_user_authenticate where user_id=:id").setParameter("id", uid).getResultList();
        if (authDatas.size() > 0) {
            XFUserAuth auth = authDatas.get(0);
            return auth.getData();
        }
        authEM.close();
        return null;
    }
}
