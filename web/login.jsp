<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="ru.pixelsky.webmc.CommonData" %>
<%@ page import="ru.pixelsky.webmc.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login, please</title>
</head>
    <body>
        <%
            User user = CommonData.getAuthorizator().getUser(request.getSession());
            if (user != null) {
                out.println("Hello, " + StringEscapeUtils.escapeHtml4(user.getLogin()) + "<br/>");
                out.println("Balance: " + user.getRealmoney() + "</br>");
                if (user.isAdmin()) {
                    out.println("[ADMIN]");
                }
            }
        %>
        <form action="login" method="post">
            <input name="login" type="text" size="20"><br/>
            <input name="password" type="password" size="20"><br/>
            <input type="submit">
        </form>

        <% if (user != null) { %>
        <form action="skincloak.set" method="post" enctype="multipart/form-data">
            Скин: <input type="file" name="skin"> <br/>
            <img src="content/skins/<%out.print(user.getLogin());%>.png" alt="skin"/>
            Плащ: <input type="file" name="cloak"><br/>
            <img src="content/cloaks/<%out.print(user.getLogin());%>.png" alt="cloak"/>
            <input type="submit">
        </form>
        <% } %>
    </body>
</html>