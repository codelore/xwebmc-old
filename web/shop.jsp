<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        // Loaded in loadShopList()
        var shopList;

        $(document).ready(loadShopList);

        function loadShopList() {
            $.ajax({
                url: "shop.list",
                dataType: "json",
                xhrFields: {
                    withCredentials: true
                },
                type: "GET",
                success: function (data) {
                    var list = data.list;

                    // Saved for further use in buy window
                    shopList = list;

                    var html = "";
                    for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        html += '<img src="' + item.image + '"/>';
                        html += item.name;
                        html += ' ' + item.price + '/штука';
                        html += '<input type="button" onclick="showBuyWindow(' + item.id + ');">';
                        html += '</br>';
                    }
                    $("#shoplist").html(html);
                }
            });
        }

        function shopItemById(id) {
            for (var i = 0; i < shopList.length; i++) {
                if (shopList[i].id == id)
                    return shopList[i];
            }
            return undefined;
        }

        function showBuyWindow(id) {
            var item = shopItemById(id);
            alert('Открываем окно для покупки ' + item.name + ' за ' + item.price + '/штука');
        }
    </script>
    <title></title>
</head>
<body>
<div id="shoplist"></div>
</body>
</html>